<?php

include("tools.php");

enteteTitreHTML("Jeu de dés !");
?>

<form method="post" action="jeu.php">
  Votre nom :

<?php
  if (isset($_COOKIE['nom']))
    echo '<input type="text" name="nom" value="' . $_COOKIE['nom'] . '"/>';
  else
    echo '<input type="text" name="nom" />';
?>

  <br />
  <input type="submit" value="C'est parti !"/>
</form>

<h1>Meilleur score utilisateur</h1>

<?php
  if (isset($_COOKIE['highscore']))
    echo "Meilleur score utilisateur : " . $_COOKIE['highscore'];
  else
    echo "Pas de meilleur score utilisateur pour l'instant..."
?>

<h1>Meilleurs scores généraux</h1>

<table border="1">
  <tr>
    <th>Numéro</th>
    <th>Nom utilisateur</th>
    <th>Score</th>
    <th>Date</th>
  </tr>

<?php
      if (isset($_GET['offset']))
	$offset = $_GET['offset'];
      else
	$offset = 0;

      $total = $pdo->query("SELECT * FROM scores;");
      $n = $total->rowCount();

      $scores = $pdo->query("SELECT * FROM scores ORDER BY score DESC LIMIT $offset, 10;");
      $scores->setFetchMode(PDO::FETCH_OBJ);

      $i = $offset + 1;

      foreach($scores as $s) {
	echo "<tr><td>$i</td><td>{$s->nom}</td><td>{$s->score}</td><td>{$s->date}</td></tr>";
	$i++;
      }
?>

</table>

<?php
if ($offset > 0)
  echo "<a href=\"{$_SERVER['PHP_SELF']}?offset=" . ($offset - 10) . "\">Page précédente</a>";

if ($n > $offset + 10)
  echo "<a href=\"{$_SERVER['PHP_SELF']}?offset=" . ($offset + 10) . "\">Page suivante</a>";
?>

<h1>Réinitialiser le meilleur score utilisateur</h1>

<form method="post" action="raz.php">
  <input type="submit" value="On efface !"/>
</form>


<?php
finHTML();
?>
