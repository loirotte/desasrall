<?php

include("tools.php");

// MAJ du meilleur score (cookie)

if (isset($_COOKIE['highscore'])) {
  if ($_COOKIE['highscore'] < $_SESSION['scoregen']) 
    setcookie('highscore', $_SESSION['scoregen'],
	      time() + 3600 * 24 * 365 * 10);
}
else {
  setcookie('highscore', $_SESSION['scoregen'],
	    time() + 3600 * 24 * 365 * 10);
  setcookie('nom', $_SESSION['nom'],
	    time() + 3600 * 24 * 365 * 10);
}

// MAJ du score dans la base

$req  = $pdo->prepare("INSERT INTO scores (nom, score) VALUES (:nom, :score);");
$maj = $req->execute(array('nom' => $_SESSION['nom'], 'score' => $_SESSION['scoregen']));

// Ménage

unset($_SESSION['tour']);
unset($_SESSION['scoregen']);
unset($_SESSION['nom']);

// Zou, on repart sur l'accueil

header("Location: index.php");
?>
